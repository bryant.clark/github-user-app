# Using this app

This GitHub user app frontend from (https://gitlab.com/bryant.clark/github-user-app-frontend) must be used in conjunction with the my github user app backend from (https://gitlab.com/bryant.clark/github-user-app)

# Getting started

Download this app as well as the backend app from above and open them in your IDE.

In your termanal run `cd node-server` then `npm i` and `npm run start -dev`

Once you've done that the server should start running and you can head over to the frondend app and get it started and see the results.