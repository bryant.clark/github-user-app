import fetch from 'node-fetch'
import cors from 'cors'
import express from 'express'
import { fetchGithubUsers } from './api/getGitHubUsers.mjs'
import { fetchFirestoreUsers } from './api/getFirestoreUsers.mjs'
import postUsers from './api/postUsers.mjs'

const app = express()
const port = process.env.PORT || 5000

app.use(cors())
app.use(express.json());


app.post('/users', async(req,res) => {
  const { input: enteredName } = req.body
  try{
    const firestoreResponse = await fetch(`https://firestore.googleapis.com/v1/projects/github-user-app-24142/databases/(default)/documents/github-users/${enteredName}`)
    const githubResponse = await fetch (`https://api.github.com/users/${enteredName}`)
    if (firestoreResponse.status === 200) {
      res.status(200).json(`Error: '${enteredName}' already entered.`)
    }
    else if (githubResponse.status === 404) {
      res.status(200).json('Error: not a valid Github username.')
    }
    else {
      const userData = await fetchGithubUsers(enteredName)
      postUsers(userData)
      res.status(200).json(`User saved.`)
    }
  } catch (error){
    return error
  }

})

app.get('/users', async(req, res) => {
  try{
  const firestoreData = await fetchFirestoreUsers()
  res.status(200).json(firestoreData)
  }
  catch(error)
  {
  res.status(500).json(error)
}}
)

app.listen(port, () => {
  console.log(`Server listening on port: ${port}`)
})
