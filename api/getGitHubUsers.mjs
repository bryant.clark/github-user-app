import fetch from 'node-fetch'

export const fetchGithubUsers = async(name) => {
  try {
  const responce = await fetch(`https://api.github.com/users/${name}`)
  const json = await responce.json()
  const userData = (makeUser(json))
  return userData  
  }
  catch(error) {
    return error
  }}

const formatDate = (date) => {
  const D = new Date(date);
  let month = D.getMonth() + 1 
  let day = D.getDate() 
  let year = D.getFullYear()
  return `${month}/${day}/${year}`
}

const makeUser = ({login, name, public_repos, public_gists, followers, following, created_at, html_url}) => {
  return{
    login: login,
    name: name,
    repos: public_repos,
    gists: public_gists,
    followers: followers,
    following: following,
    created: formatDate(created_at),
    url: html_url
  }}
