import { getFirestore, doc, setDoc } from 'firebase/firestore'

const firestore = getFirestore()


const postUsers = (userData) => {
    const githubUsersDoc = doc(firestore, 'github-users', `${userData.login.toLowerCase()}`)
    setDoc(githubUsersDoc, userData, {merge: true})
}

export default postUsers