// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDhsgLPAhkRrJqUhrvsGnHjbW6eEy20vwc",
  authDomain: "github-user-app-24142.firebaseapp.com",
  projectId: "github-user-app-24142",
  storageBucket: "github-user-app-24142.appspot.com",
  messagingSenderId: "766932614932",
  appId: "1:766932614932:web:fbe2944f49199abbde4c01",
  measurementId: "G-BXMLJYZZZ6"
};
//TODO: Add to env file

//Initialize Firebase
initializeApp(firebaseConfig);

export default getFirestore()
